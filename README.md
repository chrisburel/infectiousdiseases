This is an implementation of a version rollout strategy for new features for a website.

Each user of the site is represented by a Person object.  As a new feature is rolled out, a given set of Persons are "infected" with that new version.  It attempts to keep the feature restricted to users that have some relationship, defined by a list of coaches and students.

### Dependencies ###
The code depends on Python 2.7, and the test suite depends on:
* pytest >=2.3.5: https://pypi.python.org/pypi/pytest/2.3.5
* mock >=1.0.1: https://pypi.python.org/pypi/mock

### Future enhancements ###
It'd be nice to have an infection model that could infect exactly a given number of people.  This involves collecting a large set of groups, and then iterating over the combinations of groups until the right sum is found.  It would be very slow.