#!/usr/bin/env python
# coding=utf-8

from mock import MagicMock, PropertyMock, patch
import pytest

from person import Person
from infection import TotalInfectionModel, walkConnectionTree
from infection import PartialInfectionModel

def makeClass(className, size, teacher=None):
    '''
    Make a set of Person objects, that are connected to one Person

    Args:
        className (str):
            A name to use for this class.  This name will be used as a
            prefix to the Person names.
        size (int):
            The number of students to place in this class
        teacher (Person=None):
            The Person to use as the teacher.  If None is supplied, one is
            created.
    '''
    if teacher is None:
        teacher = Person('{0} Teacher'.format(className))
    for studentNum in xrange(1, size + 1):
        student = Person('{0} Student {1}'.format(className, studentNum))
        teacher.addStudent(student)
    return teacher

@pytest.fixture
def simpleConnectionTree():
    # Connection graph looks like this
    #        ┌──────────────────────────────────────────────────────────────────────────────┐
    #        │         ┌─Physics Student 1──Trigonometry Teacher──┬─Trigonometry Student 1  │
    # Physics Teacher──┼─Physics Student 2                      ┌─┼─Trigonometry Student 2  │
    #                  └─Physics Student 3     Biology Teacher──┴─┴─Trigonometry Student 3──┘
    physicsTeacher = makeClass('Pyhsics', 3)

    trigTeacher = physicsTeacher.students[0]
    makeClass('Trig', 3, trigTeacher)

    bioTeacher = Person('Biology Teacher')
    for student in trigTeacher.students[0:2]:
        bioTeacher.addStudent(student)

    trigTeacher.students[2].addStudent(physicsTeacher)
    return physicsTeacher

class TestTotalInfection(object):

    def testSimpleInfection(self):
        # I'm not sure why Bob and Alice always appear in code
        bob = Person('Bob')
        alice = Person('Alice')
        bob.addStudent(alice)

        assert bob.immunityLevel == 1
        assert alice.immunityLevel == 1

        infectionModel = TotalInfectionModel()
        infectionModel.infect(bob, 'immunityLevel', 2)

        assert bob.immunityLevel == 2
        assert alice.immunityLevel == 2

    def testSpreadDoesntStopIfValueAlreadySet(self):
        # Connection graph looks like this:
        #      Bob(1)          Stephen (1)
        #    /       \       /
        # Alice(1)     Sarah (2)
        # Infecting Bob with 2 should update Alice and Stephen too.
        bob = Person('Bob')
        alice = Person('Alice')
        sarah = Person('Sarah')
        stephen = Person('Stephen')

        bob.addStudent(alice)
        bob.addStudent(sarah)
        sarah.addStudent(stephen)

        sarah.immunityLevel = 2

        assert bob.immunityLevel == 1
        assert alice.immunityLevel == 1
        assert sarah.immunityLevel == 2
        assert stephen.immunityLevel == 1

        infectionModel = TotalInfectionModel()
        infectionModel.infect(bob, 'immunityLevel', 2)

        assert bob.immunityLevel == 2
        assert alice.immunityLevel == 2
        assert sarah.immunityLevel == 2
        assert stephen.immunityLevel == 2

    def testComplexInfection(self):
        # This test ensures that infecting the Physics Teacher will infect
        # everyone in the graph
        # Connection graph looks like this
        #        ┌──────────────────────────────────────────────────────────────────────────────────────┐
        #        │                                                                                      │
        #        │            ┌─Physics Student  1                                                      │
        #        │            ├─Physics Student  2                                                      │
        #        │            ├─Physics Student  3                                                      │
        #        │            ├─Physics Student  4                                                      │
        #        │            ├─Physics Student  5                                                      │
        #        │            ├─Physics Student  6                                                      │
        # Physics Teacher─────┼─Physics Student  7                                                      │
        #                     ├─Physics Student  8                                                      │
        #                     ├─Physics Student  9                                                      │
        #                     ├─Physics Student 10                                                      │
        #                     ├─Physics Student 11                            ┌─Trigonometry Student  1 │
        #                     ├─Physics Student 12                            ├─Trigonometry Student  2 │
        #                     ├─Physics Student 13   Trigonometry Teacher─────┼─Trigonometry Student  3 │
        #                     ├─Physics Student 14                            ├─Trigonometry Student  4 │
        #                     ├─Physics Student 15                            ├─Trigonometry Student  5 │
        #                  ┌──┼─Physics Student 16                            ├─Trigonometry Student  6 │
        #                  ├──┼─Physics Student 17                            ├─Trigonometry Student  7 │
        #                  ├──┼─Physics Student 18                            ├─Trigonometry Student  8 │
        # Calculus Teacher─┼──┼─Physics Student 19                            ├─Trigonometry Student  9 │
        #                  ├──┼─Physics Student 20                            ├─Trigonometry Student 10─┘
        #                  ├──┼─Physics Student 21                            ├─Trigonometry Student 11
        #                  ├──┼─Physics Student 22                         ┌──┼─Trigonometry Student 12
        #                  ├──┼─Physics Student 23                         ├──┼─Trigonometry Student 13
        #                  ├──┼─Physics Student 24                         ├──┼─Trigonometry Student 14
        #                  └──┴─Physics Student 25─────────────────────────┴──┴─Trigonometry Student 15

        # Make a Physics class with 25 students
        physicsTeacher = makeClass('Physics', 25)

        # 10 of those students are in the same Calculus class
        calculusTeacher = Person('Calculus Teacher')
        for student in list(physicsTeacher.connections)[0:10]:
            calculusTeacher.addStudent(student)

        # Make a Trigonometry class with 15 students
        trigTeacher = makeClass('Trigonometry', 15)

        # One of the students in the calculus class is tutoring 4 other people
        # in Trigonometry.  The entire Trig class gets infected.
        trigTutor = list(calculusTeacher.connections)[0]
        for student in list(trigTeacher.connections)[0:4]:
            trigTutor.addStudent(student)

        # Just to make our graph more interesting...
        trigTeacher.connections[10].addStudent(physicsTeacher)

        everybody = set()
        everybody.add(physicsTeacher)
        everybody.add(calculusTeacher)
        everybody.add(trigTeacher)
        everybody.update(physicsTeacher.connections)
        everybody.update(trigTeacher.connections)

        infectionModel = TotalInfectionModel()
        infectionModel.infect(physicsTeacher, 'immunityLevel', 2)

        for person in everybody:
            assert person.immunityLevel == 2

    def testNumberOfSetattrs(self):
        # This test ensures that each Person in the graph is only infected
        # once.

        # Make a Physics class with 25 students
        physicsTeacher = makeClass('Physics', 25)

        # 10 of those students are in the same Calculus class
        calculusTeacher = Person('Calculus Teacher')
        for student in list(physicsTeacher.connections)[0:10]:
            calculusTeacher.addStudent(student)

        everybody = set()
        everybody.add(physicsTeacher)
        everybody.add(calculusTeacher)
        everybody.update(physicsTeacher.connections)

        # Set up mock immunityLevel property to track call count
        with patch('person.Person.immunityLevel', new_callable=PropertyMock) as mockImmunityLevel:
            mockImmunityLevel.return_value = 1
            infectionModel = TotalInfectionModel()
            infectionModel.infect(physicsTeacher, 'immunityLevel', 2)

        # Ensure that each Person's attribute was only set once.
        assert len(everybody) == mockImmunityLevel.call_count

class TestWalkConnectionTree(object):
    def testTraversalIsBreadthFirst(self):
        'Ensure that walkConnectionTree is a breadth-first search'

        bob = Person('Bob')

        alice = Person('Alice')
        frank = Person('Frank')
        stephen = Person('Stephen')

        julius = Person('Julius')

        josh = Person('Josh')
        mary = Person('Mary')

        bob.addStudent(alice)
        bob.addStudent(frank)
        bob.addStudent(stephen)
        frank.addStudent(julius)
        julius.addStudent(josh)
        julius.addStudent(mary)

        for (i, person) in enumerate(walkConnectionTree(bob)):
            if i == 0:
                assert person is bob
            elif i in (1, 2, 3):
                assert person in (alice, frank, stephen)
            elif i == 4:
                assert person is julius
            elif i in (5, 6):
                assert person in (josh, mary)

class TestPartialInfection(object):
    def testGrouping(self, simpleConnectionTree):
        infectionModel = PartialInfectionModel(0)
        groups = list(infectionModel.groups(simpleConnectionTree))

        assert len(groups) == 4

        physicsTeacher = simpleConnectionTree
        trigTeacher = physicsTeacher.students[0]
        bioTeacher = trigTeacher.students[0].coaches[1]

        assert groups[0] == set([physicsTeacher] + physicsTeacher.students)
        assert groups[1] == set([trigTeacher] + trigTeacher.students)
        assert groups[2] == set([physicsTeacher] + physicsTeacher.coaches)
        assert groups[3] == set([bioTeacher] + bioTeacher.students)

    @pytest.mark.parametrize(('limit', 'slices'), (
        (
            10,
            [[0,10]]
        ),
        (
            9,
            [[0,9]]
        ),
        (
            8,
            [[0,8]]
        ),
        (
            7,
            [[0,7]]
        ),
        (
            # Asking to infect 6 people will infect 2 groups:
            # Physics Teacher and her 3 students,
            # and the Physics Teacher and her coach.
            6,
            [[0,5]]
        ),
        (
            5,
            [[0,5]]
        ),
        (
            4,
            [[0,4]]
        ),
        (
            # Asking to infect 3 people will only infect 2, the Physics Teacher
            # and her coach.
            3,
            [[0,1], [4,5]]
        ),
        (
            2,
            [[0,1], [4,5]]
        ),
        (
            # Everyone has at least one connection, so there's no group
            # containing just one Person.
            1,
            []
        ),
    ))
    def testPartialInfection(self, simpleConnectionTree, limit, slices):
        # This test tests infecting the people from the simpleConnectionTree
        # with different infection limits.  The limit argument defines the max
        # number of Persons that will be infected.  We know that all the
        # Persons infected can be represented as some combination of slices of
        # the list returned from walking the connection tree of
        # simpleConnectionTree.  This is what the slices argument represents.

        infectionModel = PartialInfectionModel(limit)
        infectionModel.infect(simpleConnectionTree, 'immunityLevel', 2)

        physicsTeacherConnections = list(walkConnectionTree(simpleConnectionTree))
        got = [[person, person.immunityLevel] for person in physicsTeacherConnections]

        expectedInfected = []
        for s in slices:
            expectedInfected.extend(physicsTeacherConnections[s[0]:s[1]])
        expected = [[person, 2 if person in expectedInfected else 1] for person in physicsTeacherConnections]

        assert got == expected
