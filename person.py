class Person(object):
    '''
    A Person class.  Person objects can connect with each other to form
    relationships.
    '''

    def __init__(self, name):
        self._name = name
        self._immunityLevel = 1

        # Keep track of what other Persons we're connected to.
        self._coaches = []
        self._students = []

    def __repr__(self):
        return '<person.Person(name={0})>'.format(repr(self.name))

    @property
    def coaches(self):
        "([Person]) This Person's coaches"
        return list(self._coaches)

    @property
    def connections(self):
        "([Person]) This Person's coaches and students"
        return list(self._coaches) + self._students

    @property
    def immunityLevel(self):
        "(int) This person's current immunity level"
        return self._immunityLevel

    @immunityLevel.setter
    def immunityLevel(self, newLevel):
        self._immunityLevel = newLevel

    @property
    def name(self):
        "(str) This person's name"
        return self._name

    @property
    def students(self):
        "([Person]) This Person's students"
        return list(self._students)

    def addStudent(self, other):
        '''
        Set `other` as a student of this Person.

        Args:
            other (Person):
                The Person to become a new student
        '''

        if other not in self._students:
            self._students.append(other)
            other._coaches.append(self)
