from abc import ABCMeta, abstractmethod

def walkConnectionTree(person):
    '''
    Iterate over a person's connection tree, breadth-first.

    Args:
        person (Person):
            The Person to start the connection tree traversal.
    '''
    people = [person]

    # Keep track of the Persons we've visited already
    seen = set()

    # Infect them one at a time
    while people:
        person = people.pop(0)

        # Make sure we haven't already visited this Person
        if person in seen:
            continue

        yield person

        seen.add(person)

        # Visit this Person's connections.  Visit students first.
        people.extend(person.students)
        people.extend(person.coaches)

class InfectionModel(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def infect(self, person, attrName, value):
        '''
        Apply an infection.

        Args:
            person (Person):
                The starting Person to infect.
            attrName (str):
                The attribute on the Person to apply the infection to
            value:
                The value to set.

        This method applies a given value to an attribute on a Person, and
        potentially other Persons that Person is connected with.
        '''

class TotalInfectionModel(InfectionModel):
    def infect(self, person, attrName, value):
        '''
        Apply an infection.

        Args:
            person (Person):
                The starting Person to infect.
            attrName (str):
                The attribute on the Person to apply the infection to
            value:
                The value to set.

        This method applies a given value to an attribute on a Person, and
        all other Persons that Person is connected with, and their connections,
        until every Person in the connection chain has that value set.
        '''

        for person in walkConnectionTree(person):
            # Apply the infection
            setattr(person, attrName, value)

class PartialInfectionModel(InfectionModel):
    '''
    This infection model implements a limited infection.  It will infect a
    maximum number of people, starting from a given person.

    This model first creates groups, and will only continue to infect people if
    it can infect everyone in the next group without going over the maximum.
    It is "The Price is Right" model, where it gets as close as it can to the
    right number of infections, without going over.
    '''
    def __init__(self, maxInfections):
        self._maxInfections = maxInfections

    @property
    def maxInfections(self):
        '(int) The maximum number of people any infect() call will affect'
        return self._maxInfections

    def groups(self, person):
        '''
        Break up this Person's connection tree into a list of Groups.

        Args:
            person (Person):
                The Person to start the tree traversal

        Groups are created merely by a given coach and that coach's students.
        '''
        for person in walkConnectionTree(person):
            if not person.students:
                continue
            group = set((person,)) | set(person.students)
            yield group

    def infect(self, person, attrName, value):
        '''
        Apply an infection.

        Args:
            person (Person):
                The starting Person to infect.
            attrName (str):
                The attribute on the Person to apply the infection to
            value:
                The value to set.

        This method applies a given value to an attribute on a Person, while
        affecting a max of `maxInfections` Persons.  It first groups the
        Persons according to groups(), and will then only infect whole groups,
        as long as the new people that would be infected by infecting the group
        is not less than `maxInfections`.
        '''

        # Find the coach->student groups in this person's connection tree
        groups = self.groups(person)

        # Define the set of Persons that will be infected
        groupToInfect = set()

        for candidateGroup in groups:
            # See if infecting this group puts us over our max infection count
            newGroupToInfect = set(groupToInfect)
            newGroupToInfect.update(candidateGroup)

            if len(newGroupToInfect) > self.maxInfections:
                # We're over the limit, keep trying other groups
                continue

            # We're under the limit, infect the people in this group
            groupToInfect = newGroupToInfect

        for person in groupToInfect:
            # Apply the infection
            setattr(person, attrName, value)
